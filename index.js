// The "document" refers to the whole webpage.
// The "querySelector" is used to select a specific object (HTML elements) from the webpage (document).

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');
/* 
	Alternatively, we can use getElement functions to retrieve the elements
	const txtFirstName = document.getElementByClass('txt-first-name');

 */ 

/* 
	// The "addEventListener" is a function that takes two arguments.
	// keyup - string identifying the event 
	// event => {} - function that the listner will execute once the specified event is triggered.
*/ 

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value; 
});

/*
		// The "innerHTML" property sets or returns the HTML content 
*/

 
txtFirstName.addEventListener('keyup',(event) => {
/*
	// The "event.target" contains the element where the event happened. 
*/
	console.log(event.target);
/*
	// The "event.target.value" gets he value of the input object.
*/
	console.log(event.target.value);
})

/*

	txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtLastName.value; 
	});

*/ 

txtFirstName.addEventListener('keyup',(event) => {
	console.log(event.target);
	console.log(event.target.value);
})